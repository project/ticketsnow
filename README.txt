README.txt
==========

A module containing API integrations for TICKETSNOW API,
It provides API for developers to use the ticketsnow web services using soap client.

###  SERVICES ###

It also provides a framework for web service integration, (SERVICES module requried)
with web service for,
-- searhing events 
-- serching productions 
already present. 

and using the API provided by the module other web services from ticketsnow can be integrated too.

COMPATIBILITY NOTES
==================
- Module requires the soap client module to be enabled

INSTRUCTIONS:
==================

1: Enable the soapclient module, (http://drupal.org/project/soapclient)
2: GO TO admin/settings/soapclient, select the Active SOAP Library: nuSOAP
3: GO TO the ticketsnow configuration page, enter all the required information regarding your ticketsnow affiliation.
4: TO USE THE LIVE Ticketsnow web services, 
   check the "use Live account option" if unchecked it will use the preview web services.
   
 
   
   
### example to use the API. ##

//create an array of arguments, where the key is the argument name of the ticketsnow web service
$required_args['CityOrZip'] = $city;

//pass the ticketsnow method name and the arguments array as input to the api call.
$all_markets = ticketsnow_call_api('SearchMarketAreas', $required_args);



AUTHOR/MAINTAINER
======================
- Swarad Mokal at BLiSStering Solutions.
