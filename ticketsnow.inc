<?php 
/**
 * Searches tickets for Events
 * @param string $event_name
 *   REQUIRED
 * @param $postal_code
 *   OPTIONAL : DEFUALT = 0
 * @param $start_date
 *   OPTIONAL : DEFUALT = NULL
 * @param $city
 *   OPTIONAL : DEFUALT = NULL
 * 
 */
function tickets_search_events($event_name, $postal_code = 0, $start_date = NULL, $city = NULL){
  $required_args = array();
  /**
   * Ticketsnow API Method : SearchMarketAreas
   */
  if($city != NULL || $city != ''){
    $required_args['CityOrZip'] = $city;
    $all_markets = ticketsnow_call_api('SearchMarketAreas', $required_args);
    $k = 0;
    if(isset($all_markets['ROOT']['DATA']['row'][$k]['!MarketAreaID'])){
    while ($all_markets['ROOT']['DATA']['row'][$k]['!MarketAreaID'] != NULL){
        $all_markets_id[] = $all_markets['ROOT']['DATA']['row'][$k]['!MarketAreaID'];
        $k++;
      }
    }
    else{
      $all_markets_id[] = $all_markets['ROOT']['DATA']['row']['!MarketAreaID'];
      $k++;
    }
  }
  /**
   * Ticketsnow API Method : SearchEvents
   */
  $required_args = array();
  if($postal_code != 0){
    $required_args['PostalCode'] = $postal_code;
  }
  if($start_date != NULL){
    $required_args['StartDate'] = $start_date;
  }
  if($all_markets_id != NULL){
    foreach ($all_markets_id as $market_id){
      $required_args['EventName'] = $event_name;
      $required_args['MarketAreaID'] = $market_id;
      $events = ticketsnow_call_api('SearchEvents', $required_args);
      if($events['ROOT']['DATA'] != NULL){
        $all_events = array();
        $j = 0;
        if(isset($events['ROOT']['DATA']['row'][$j]['!EventID'])){
          drupal_set_message("ss");
          while ($events['ROOT']['DATA']['row'][$j]['!EventID'] != NULL){
            $all_events[$j]['EventID'] = $events['ROOT']['DATA']['row'][$j]['!EventID'];
            $all_events[$j]['EventName']= $events['ROOT']['DATA']['row'][$j]['!EventName'];
            $all_events[$j]['MinDate']= $event['ROOT']['DATA']['row'][$j]['!MinDate'];
            $all_events[$j]['MaxDate']= $event['ROOT']['DATA']['row'][$j]['!MaxDate'];
            $j++;
          }
        }
        else{
          $all_events[$j]['EventID'] = $events['ROOT']['DATA']['row']['!EventID'];
          $all_events[$j]['EventName']= $events['ROOT']['DATA']['row']['!EventName'];
           $all_events[$j]['MinDate']= $event['ROOT']['DATA']['row']['!MinDate'];
            $all_events[$j]['MaxDate']= $event['ROOT']['DATA']['row']['!MaxDate'];
          $j++;
        }
      }
    }
    if($all_events != NULL){
      return $all_events;
    }
    else{
      $output['return_code'] = 1;
      $output['return_message'] = 'No tickets found for this event';
      return $output;
    }
 }
  else{
    //do not use market id
    $required_args['EventName'] = $event_name;
    $event = ticketsnow_call_api('SearchEvents', $required_args);
    if($event['ROOT']['DATA'] != NULL){
      $all_events = array();
      $j = 0;
      if(is_array($event['ROOT']['DATA']['row'][$j])){
        while ($event['ROOT']['DATA']['row'][$j]['!EventID'] != NULL){
          $all_events[$j]['EventID'] = $event['ROOT']['DATA']['row'][$j]['!EventID'];
          $all_events[$j]['EventName']= $event['ROOT']['DATA']['row'][$j]['!EventName'];
          $j++;
        }
      }
      else{
        $all_events[$j]['EventID'] = $event['ROOT']['DATA']['row']['!EventID'];
        $all_events[$j]['EventName']= $event['ROOT']['DATA']['row']['!EventName'];
        $j++;
      }
    }
    if($all_events != NULL){
      return $all_events;
    }
    else{
      $output['return_code'] = 1;
      $output['return_message'] = 'No tickets found for this event';
      return $output;
    }
  }
}
/**
 * Search productions for the event selecetd
 * @param int $event_id
 */
function tickets_search_productions($event_id){
  $required_args = array();
  $required_args['EventID'] = $event_id;
  $event = ticketsnow_call_api('SearchProductions', $required_args);
  if($event == FALSE){
    $output['return_code'] = 2;
    $output['return_message'] = 'No tickets found for this event';
    return $output;
  }
  $all_productions = array();
  $i = 0;
  //fetch the production id and other details
  while ($event['ROOT']['DATA']['row'][$i]['!ProductionID'] != NULL){
    $all_productions[$i]['EventID'] = $event['ROOT']['DATA']['row'][$i]['!EventID'];
    $all_productions[$i]['EventName']= $event['ROOT']['DATA']['row'][$i]['!EventName'];
    $all_productions[$i]['ProductionID']= $event['ROOT']['DATA']['row'][$i]['!ProductionID'];
    $all_productions[$i]['VenueID']= $event['ROOT']['DATA']['row'][$i]['!VenueID'];
    $all_productions[$i]['VenueName']= $event['ROOT']['DATA']['row'][$i]['!VenueName'];
    $all_productions[$i]['EventDate']= $event['ROOT']['DATA']['row'][$i]['!EventDate'];
    $i++;
  }
   return $all_productions;
}